#include "my_list.h"

int			l_size(t_list *begin)
{
  int		result;

  while (begin != 0)
  {
    begin = begin->next;
    result++;
  }
  return (result);
}

t_list		*create(void *data, t_list *next)
{
    t_list 	*new_node;

    new_node = (t_list*)malloc(sizeof(new_node));
    if (new_node == NULL)
        return NULL;
    new_node->data = data;
    new_node->next = next;
    return new_node;
}

t_list		*append(t_list *head, void *data)
{
    /* go to the last node */
    t_list *cursor;

    cursor = head;
    while (cursor->next != NULL)
        cursor = cursor->next;
    t_list *new_node = create(data, NULL);
    cursor->next = new_node;
    return (head);
}

t_list 		*add_head(t_list *l, void *data)
{
  t_list 	*nl;
 
  nl = malloc(sizeof(*nl));
  if (nl == 0)
    return (NULL);
  nl->data = data;
  nl->next = l;
  return (nl);
}

t_list		*my_params_in_list(int argc, char **argv)
{
  int		i;
  t_list	*list;

  list = malloc(sizeof(t_list));
  list->data = argv[0];
  list->next = NULL;
  i = 1;
  while (i < argc)
  {
    list = add_head(list, argv[i]);
    i++;
  }
  return (list);
}

void  		my_apply_on_list(t_list *begin, int (*f)())
{
  while (begin)
  {
    f(begin->data);
    begin = begin->next;
  }
}

void		my_rev_list(t_list **begin)
{
  t_list	*prev;
  t_list	*current;
  t_list	*next;

  prev = NULL;
  current = *begin;
  while (current != NULL)
    {
      next = current->next;
      current->next = prev;
      prev = current;
      current = next;
    }
  *begin = prev;
}

t_list  	*my_find_node_elm_eq_in_list(t_list *begin
				     , void *data_ref
				     , int (*cmp)())
{
  t_list	*tmp;

  tmp = begin;
  while (tmp && cmp(tmp->data, data_ref) != 0)
      tmp = tmp->next;
  return (tmp != NULL ? tmp : NULL);
}